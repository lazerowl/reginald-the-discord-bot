import logging


class Config:
    IGNORE_FILE_PATH = "./ignore"
    LOG_LEVEL = logging.DEBUG
