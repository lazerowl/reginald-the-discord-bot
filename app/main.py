#!/usr/bin/env python3

import logging
import os
import random
from typing import Optional

import discord
from config import Config
from discord import app_commands
from dotenv import load_dotenv

logger = logging.getLogger("discord")
logging.basicConfig(level=Config.LOG_LEVEL)

if not load_dotenv():
    raise Exception("Failed to load dotenv.")

TOKEN = os.getenv("DISCORD_TOKEN")
if not TOKEN:
    raise Exception("No Discord token set.")

GUILD_ID = os.getenv("DISCORD_GUILD_ID")
if not GUILD_ID:
    raise Exception("No Discord guild ID set.")
GUILD_ID = int(GUILD_ID)
GUILD_OBJECT = discord.Object(id=GUILD_ID)

intents = discord.Intents.default()
intents.members = True
intents.presences = True
client = discord.Client(intents=intents)
tree = app_commands.CommandTree(client)

ignored_members = set()

try:
    with open(Config.IGNORE_FILE_PATH, "r") as infile:
        logger.debug("Reading ignore list:")
        for line in infile:
            line = line.strip()
            logger.debug(line)
            ignored_members.add(line)
except FileNotFoundError:
    with open(Config.IGNORE_FILE_PATH, "w"):
        pass


@tree.command(name="teams", description="Randomize two teams from all online channel members", guild=GUILD_OBJECT)
async def teams(interaction: discord.Interaction, players: Optional[str] = None):
    if not interaction.channel_id:
        raise Exception("No interaction channel ID.")
    channel = client.get_channel(interaction.channel_id)

    if not isinstance(channel, discord.TextChannel):
        raise Exception("Command was not invoked from a text channel.")

    if players:
        playing_members = players.split()
    else:
        playing_members = []
        for member in channel.members:
            member_nick = member.nick or member.name
            if member.status is discord.Status.online and member_nick not in ignored_members:
                playing_members.append(member_nick)
    random.shuffle(playing_members)

    split_index = len(playing_members) // 2
    team_a = playing_members[split_index:]
    team_b = playing_members[:split_index]

    if len(team_b) < len(team_a):
        team_b.append("Reginald")

    team_a = ", ".join(team_a)
    team_b = ", ".join(team_b)

    factions = ["Axis", "Allies"]
    random.shuffle(factions)
    faction_a = factions[0]
    faction_b = factions[1]

    message_lines = [f"Team A: {team_a} ({faction_a})", f"Team B: {team_b} ({faction_b})"]
    message = "\n".join(message_lines)

    await interaction.response.send_message(message)


@tree.command(name="tally", description="Report match result", guild=GUILD_OBJECT)
async def tally(interaction: discord.Interaction, players: str):
    # for player in players:
    #   database.add_victory(player, faction)
    pass


@tree.command(name="ignore", description="Ignore channel member when randomizing teams", guild=GUILD_OBJECT)
async def ignore(interaction: discord.Interaction, member_name: str):
    if not interaction.channel_id:
        raise Exception("No interaction channel ID.")
    channel = client.get_channel(interaction.channel_id)

    if not isinstance(channel, discord.TextChannel):
        raise Exception("Command was not invoked from a text channel.")

    members = [member.nick or member.name for member in channel.members]
    if member_name not in members:
        message = f'Error: Unknown member "{member_name}"'
    else:
        ignored_members.add(member_name)
        with open(Config.IGNORE_FILE_PATH, "w") as outfile:
            for member in ignored_members:
                outfile.write(f"{member}\n")
        message = f"{member_name} will be ignored when randomizing teams."

    await interaction.response.send_message(message)


@tree.command(name="unignore", description="Stop ignoring channel member", guild=GUILD_OBJECT)
async def unignore(interaction: discord.Interaction, member_name: str):
    try:
        ignored_members.remove(member_name)
        with open(Config.IGNORE_FILE_PATH, "w") as outfile:
            for member in ignored_members:
                outfile.write(f"{member}\n")
    except ValueError:
        message = f"Error: {member_name} was not in ignore list."
    else:
        message = f"{member_name} will no longer be ignored when randomizing teams."

    await interaction.response.send_message(message)


@tree.command(name="show_ignore_list", description="Show ignore list", guild=GUILD_OBJECT)
async def show_ignore_list(interaction: discord.Interaction):
    message_lines = [member for member in ignored_members]
    message = "\n".join(message_lines)

    if not message:
        message = "Ignore list is empty."

    await interaction.response.send_message(message)


@tree.command(name="clear_ignore_list", description="Clear ignore list", guild=GUILD_OBJECT)
async def clear_ignore_list(interaction: discord.Interaction):
    ignored_members.clear()
    os.unlink(Config.IGNORE_FILE_PATH)
    await interaction.response.send_message("Ignore list cleared.")


@client.event
async def on_ready():
    await tree.sync(guild=GUILD_OBJECT)
    logger.info("Reginald is ready to serve!")


if __name__ == "__main__":
    client.run(TOKEN)
